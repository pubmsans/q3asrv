
# Quake3 Arena Server Configuration Generator and Launcher

## Server Configuration Generator: q3asrv.py

### Manual

```
usage: q3asrv.py [options] MOD

q3asrv.py is a wrapper for ioq3ded

positional arguments:
  MOD                   quake3 mod

options:
  -h, --help            show this help message and exit
  -S [SERVER_CONFIG], --server-cfg [SERVER_CONFIG]
                        server config
  -G [GAME_CONFIG], --game-cfg [GAME_CONFIG]
                        game config
  --ffa-cfg [FFA_CONFIG]
                        ffa config
  --trn-cfg [TRN_CONFIG]
                        trn config
  --tdm-cfg [TDM_CONFIG]
                        tdm config
  --ctf-cfg [CTF_CONFIG]
                        ctf config
  --mods-cfg [MODS_CONFIG]
                        mods config
  -M [MAPGROUPMODS_CONFIG], --mapgroupmods-cfg [MAPGROUPMODS_CONFIG]
                        map group mods config
  -C [BOTS_CONFIG], --bots-cfg [BOTS_CONFIG]
                        bots config
  -T [BOTSELECTIONS_CONFIG], --botselections-cfg [BOTSELECTIONS_CONFIG]
                        bot selections config
  -g [{ctf,tdm,ffa,trn}], --game-type [{ctf,tdm,ffa,trn}]
                        game type
  -m [q3dm1,...], --maps [q3dm1,...]
                        maps
  -n [0-16], --bot-minplayers [0-16]
                        minimal bots number (no effect with -B, -R, or -F)
  -s [1-5], --g-spskill [1-5]
                        bots skills (g_spskill)
  -d [0-59], --bots-delay [0-59]
                        Delay bots entry in sec.
  -F [0-16], --max-free [0-16]
                        Maximum number of free bots
  -B [0-16], --max-blue [0-16]
                        Maximum number of blue bots
  -R [0-16], --max-red [0-16]
                        Maximum number of red bots
  -f [crash,doom,...], --free [crash,doom,...]
                        Free bot or bot selection.
  -b [crash,doom,...], --blueteam [crash,doom,...]
                        Bot or bot selection joinging blue team.
  -r [crash,doom,...], --redteam [crash,doom,...]
                        Bot or bot bot selection joinging red team.

```

### Configuration Files

#### Server Options

Default server configuration is defined in `~/.q3asrv/server.cfg`. E.g.:

```
set dedicated 1                 // Dedicated server but not announced
set com_hunkmegs 512            // How much RAM for your server
seta r_smp "1"                  // whether the server has multiple CPUs
set net_port 27960              // The network port
seta rate "25000"               // connexion (see http://quake3tweaks.tripod.com/commands.html )
seta snaps "40"                 //
seta cl_maxpackets "200"        // WLAN: 100, LAN: 200
seta cl_packetdup "0"           // WLAN: "1", lAN: "0"
seta g_log server.log           // log
seta logfile 3                  //
seta rconpassword "quake3"      // sets RCON password for remote console
set s_musicVolume 0             // shutdown music
seta sv_strictauth "0"          // whether CD-key is checked on client
set vm_game 2                   // WTF magic tricks (http://zerowing.idsoftware.com/linux/q3a/)
set vm_cgame 2                  // 
set vm_ui 2                     // 
seta sv_maxclients 24           // max number of clients than can connect
seta sv_pure 1                  // server, no altered pak files
set ".Admin" "BOFL"      // admin info
seta sv_hostname "SpongeJim Frag&Fun" // name that appears in server list
```
Typically, if you want to run the server on the loopback only, you can pass another server configuration as argument (option `S`), say `~/.q3asrv/localhost_server.cfg`, that additionally contains the directive:

```
set net_ip 127.0.0.1            // The network ip
```

See also: [quake3world.com](https://www.quake3world.com/q3guide/servers.html), [quake.fandom.com](https://quake.fandom.com/wiki/Console_Commands_(Q3)), [quake3tweaks.tripod.com](https://quake3tweaks.tripod.com/commands.html) 

#### Game and Game Types Options

Default main game configuration is defined in `~/.q3asrv/game.cfg`. E.g.

```
seta g_quadfactor 3             // quad damage strength (3 is normal)
seta g_friendlyFire 1           // friendly fire motherfucker
seta g_weaponrespawn 2          // weapon respawn in seconds 
seta g_inactivity 300           // kick players after being inactive for x seconds
seta g_forcerespawn 0           // player has to press primary button to respawn
```

The game types configuration are defined in 

* `~/.q3asrv/ffa.cfg` (Free For All)
* `~/.q3asrv/trn.cfg` (Tourney)
* `~/.q3asrv/tdm.cfg` (Team Deathmatch)
* `~/.q3asrv/ctf.cfg` (Capture The Flag)

E.g. for `ffa.cfg`:

```
seta g_motd "Q3A Free For All"       // message that appears when connecting
seta g_gametype 2               // 0:FFA, 1:Tourney, 2:FFA, 3:TD, 4:CTF
seta timelimit 10               // Time limit in minutes
seta fraglimit 30               // Frag limit
```

and `ctf.cfg`:

```
seta g_motd "Q3A Capture The Flag"       // message that appears when connecting
seta g_gametype 4               // 0:FFA, 1:Tourney, 2:FFA, 3:TD, 4:CTF
seta timelimit 10               // Time limit in minutes
seta capturelimit 5             // Flag capture limit
seta fraglimit 0                // Frag limit
seta g_teamAutoJoin 0           // 0:goes into spectator mode, 1:auto joins a team
seta g_teamForceBalance 1       // 0:free selection, 1:forces player to join weak team
```

See also: [quake3world.com](https://www.quake3world.com/q3guide/servers.html), [quake.fandom.com](https://quake.fandom.com/wiki/Console_Commands_(Q3)), [quake3tweaks.tripod.com](https://quake3tweaks.tripod.com/commands.html)

#### Additional Bots Definition 
 
Default additional bots definition file is stored in `~/.q3asrv/bots.toml`. It describes the additional bots available and defines if they are playable in teams, e.g.

```toml
[lara]
team = true
comments = "laracroft"

[stormtrooper]
team = true
comments = ""

[sandtrooper]
team = true
comments = ""

[ultramarine]
team = true
comments = "spacemarine40k"

[bloodangel]
team = true
comments = "spacemarine40k"

[crimsonfist]
team = true
comments = "spacemarine40k"

[darkangel]
team = true
comments = "spacemarine40k"

[dipsy]
team = false
comments = ""

[po]
team = false
comments = ""

[tinky-winky]
team = false
comments = ""

[laa-laa]
team = false
comments = ""

[jarjar]
team = false
comments = ""

[alien3]
team = true
comments = ""
```
### Playable Custom Mods

Default configuration file for playable custom mods is defined in `~/.q3asrv/mod.toml`. It contains the mapping between mods and gametypes, e.g:

```toml
alternatefire = ["ffa","trn","tdm","ctf"]
q3ctc = ["tdm","ctf"]
```

### Map Group Mods

Default configuration file for map group mods is defined in `~/.q3asrv/mapgroupmods.toml`. It contains:

* a section per map group mode: the section name defines the mod name
* the base mode (key `basemode`) e.g. `alternatefire`
* the compatible game types )key `gametypes`)
* a `variantmods` sections listing the compatible alternative mods (mod as key and value as mod name (i.e. installatin folder name) extension suffix).
* a `maps` subsection listing the contained maps (mapname as key and some description as value)

E.g.
```toml
[_std_high]
defaultexpand   = true
basemod         = "alternatefire"
gametypes       = [ "tdm", "ffa", "trn",]
[_std_high.variantmods]
baseq3 = "def"

[_std_high.maps]
berserk                 = "map-berserk, 4/4"
twq3tourney2            = "4/4"
unavi                   = "4/4"
wmgdm1                  = "4/4 (greek temple)"
ravedm2                 = "4/4"
madddm2                 = "4/4"
platform6               = "4/4"
darkfortress            = "4/4"
```

### Teams

The default configuration file for pre-selection of bots is defined in `~/.q3asrv/botselections.toml`. It contains:

* a section per bot selection: the section name defines the bot selection name
* a parameter `members` (array) containing each bot that is a selection member
* each member is defined by
    * a subsection which name defines the bot nickname
    * a parameter `bot`: the name of bot, e.g. `crash`
    * a parameter `deltaskill`: the delta skill of the bot (increment or decrement from the global bot skill option)
* a parameter `repeat`: this parameter allows you to create arbitrary large team, by repeating the team members (appending an index at their nicknames if repetition is necessary)

E.g.

```toml
[doomcrashteam]
repeat = true

[doomcrashteam.members.Crash]
bot = "crash"
deltaskill = 1

[doomcrashteam.members.Doom]
bot = "doom"
deltaskill = 0

[spacemarines]
repeat = false

[spacemarines.members.Alpha]
bot = "ultramarine"
deltaskill = 0

[spacemarines.members.Beta]
bot = "bloodangel"
deltaskill = 0

[spacemarines.members.Gamma]
bot = "crimsonfist"
deltaskill = 0

[spacemarines.members.Delta]
bot = "darkangel"
deltaskill = 0

[spacemarines.members.Epsilon]
bot = "ultramarine"
deltaskill = 0

[spacemarines.members.Theta]
bot = "bloodangel"
deltaskill = 0

[spacemarines.members.Omicron]
bot = "crimsonfist"
deltaskill = 0

[spacemarines.members.Pi]
bot = "darkangel"
deltaskill = 0
```

## Server Launcher: q3arun.sh

### Configuration Files

Define the unix flavor of the system running `q3asrv.py` in `~/.q3a/.unix_falvor`. Accepted directives are

```
arch
```
for Archlinux and
```
debian
```
for Debian GNU/Linux based distributions

