#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pathlib
import argparse
import re
import math
import random
import tomli
import time

# argument parsing
p_bs = re.compile(r'^=[1-5]$|^[+\-][0-4]$') # pattern to match bot skills setting, bot configuration
p_mf = re.compile(r'^[1-9]{1,1}[0-9]?$') # pattern to match multiplier factor, bot/team configuration
def get_q3asrv_config() -> dict:
# configuration
    cfg = dict()
    cfg["gametypes_teams_builtin"] = set(["tdm", "ctf"])
    cfg["gametypes_nonteams_builtin"] = set(["ffa", "trn"])
    cfg["path_q3asrv"] = (pathlib.Path.home()).joinpath(".q3asrv")
    cfg["path_q3a"] = (pathlib.Path.home()).joinpath(".q3a")
    cfg["file_fs_game"] = cfg["path_q3a"].joinpath(".fs_game.q3asrv")
    cfg["file_customserver"] = pathlib.Path("customserver.cfg")
    cfg["mod_default"] = 'baseq3'
    cfg["gametypes_builtin"] = cfg["gametypes_teams_builtin"] | cfg["gametypes_nonteams_builtin"]
    cfg["bots_builtin"] = ('anarki', 'angel', 'biker', 'bitterman', 'bones', 'cadavre', 'crash', 'daemia',
                                 'doom', 'gorre', 'grunt', 'hossman', 'hunter', 'keel', 'klesk', 'lucy',
                                 'major', 'mynx', 'orbb', 'patriot', 'phobos', 'ranger', 'razor', 'sarge',
                                 'slash', 'sorlag', 'stripe', 'tankJr', 'uriel', 'visor', 'wrack', 'xaero')
    cfg["bots_max_nb"] = 16
    cfg["maps_default"] = "q3ctf1",
    cfg["maps_builtin"] = dict.fromkeys(list(cfg["gametypes_builtin"]), None)
    cfg["maps_builtin"]["ctf"] = ['q3ctf' + str(k) for k in list(range(1, 5))]
    cfg["maps_builtin"]["ctf"].extend(['q3tourney6_ctf'])
    cfg["maps_builtin"]["ffa"] = cfg["maps_builtin"]["ctf"][:]
    cfg["maps_builtin"]["ffa"].extend(['q3dm' + str(k) for k in list(range(0, 20))])
    cfg["maps_builtin"]["ffa"].extend(['q3tourney' + str(k) for k in list(range(1, 7))])
    cfg["maps_builtin"]["ffa"].extend(['pro-q3dm6', 'pro-q3dm13', 'pro-q3tourney2', 'pro-q3tourney4'])
    cfg["maps_builtin"]["tdm"] = cfg["maps_builtin"]["ffa"]
    cfg["maps_builtin"]["trn"] = cfg["maps_builtin"]["ffa"]
    cfg["configuration_nongames_files"] = ["server", "game"]
    cfg["configuration_files"] = tuple(cfg["configuration_nongames_files"] + list(cfg["gametypes_builtin"]))
    cfg["files_directives"] = dict(zip(
        cfg["configuration_files"],
        [cfg["path_q3asrv"].joinpath(f"{x}.cfg") for x in cfg["configuration_files"]]))  # default location
    cfg["customization_files"] = ("mods", "mapgroupmods", "bots", "botselections")
    cfg["files_definitions"] = dict(zip(
        cfg["customization_files"],
        [cfg["path_q3asrv"].joinpath(f"{x}.toml") for x in cfg["customization_files"]]))  # default location

    return cfg

def write_cfg_file(filename: str, lines: list) -> None:
    with open(filename, "w") as f:
        f.write("\n".join(lines))
    return None

# read cfg file
def read_cfg_file(filename: str) -> list:
    with open(filename) as f:
        fcontent = f.readlines()
    return fcontent

# read toml file
def read_toml_file(filename: str) -> dict:
    with open(filename, 'rb') as f:
        fcontent = tomli.load(f)
    return fcontent
def argument_parsing(cfg: dict) -> argparse.Namespace:
    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] MOD"
    description = script_path.name + " is a wrapper for ioq3ded"
    parser = argparse.ArgumentParser(usage=usage, description=description)

    # server configuration
    parser.add_argument("-S", "--server-cfg", help="server config", dest="server_config",
                        default=cfg["files_directives"]["server"], action="store", nargs="?")
    # game configuration
    parser.add_argument("-G", "--game-cfg", help="game config", dest="game_config",
                        default=cfg["files_directives"]["game"], action="store", nargs="?")
    # free-for-all gametype configuration
    parser.add_argument("--ffa-cfg", help="ffa config", dest="ffa_config",
                        default=cfg["files_directives"]["ffa"], action="store", nargs="?")
    # tourney gametype configuration
    parser.add_argument("--trn-cfg", help="trn config", dest="trn_config",
                        default=cfg["files_directives"]["trn"], action="store", nargs="?")
    # team death-match gametype configuration
    parser.add_argument("--tdm-cfg", help="tdm config", dest="tdm_config",
                        default=cfg["files_directives"]["tdm"], action="store", nargs="?")
    # capture-the-flag gametype configuration
    parser.add_argument("--ctf-cfg", help="ctf config", dest="ctf_config",
                        default=cfg["files_directives"]["ctf"], action="store", nargs="?")
    # mods customization
    parser.add_argument("--mods-cfg", help="mods config", dest="mods_config",
                        default=cfg["files_definitions"]["mods"], action="store", nargs="?")
    # maps per mod customizations
    parser.add_argument("-M", "--mapgroupmods-cfg", help="map group mods config", dest="mapgroupmods_config",
                        default=cfg["files_definitions"]["mapgroupmods"], action="store", nargs="?")
    # bots customization
    parser.add_argument("-C", "--bots-cfg", help="bots config", dest="bots_config",
                        default=cfg["files_definitions"]["bots"], action="store", nargs="?")
    # teams(bots selections) customization
    parser.add_argument("-T", "--botselections-cfg", help="bot selections config", dest="botselections_config",
                        default=cfg["files_definitions"]["botselections"], action="store", nargs="?")
    # game type choice
    parser.add_argument("-g", "--game-type", help="game type", dest="game_type",
                        action="store", nargs="?",
                        choices=list(cfg["gametypes_builtin"]), default="ffa")
    # map(s) choise
    parser.add_argument("-m", "--maps", help="maps",
                        action="append", nargs="?", default=[], metavar="q3dm1,...")
    # minimal number of players/team-members to complete with bots
    parser.add_argument("-n", "--bot-minplayers", help="minimal bots number (no effect with -B, -R, or -F)",
                        dest="bot_minplayers", action="store", default=0, type=int,
                        choices=list(range(0, cfg["bots_max_nb"] + 1)), metavar="0-" + str(cfg["bots_max_nb"]),
                        nargs="?", )
    # default bots skills
    parser.add_argument("-s", "--g-spskill", help="bots skills (g_spskill)", dest="g_spskill",
                        action="store", default=2, type=int, choices=list(range(1, 6)), metavar="1-5",
                        nargs="?")
    # delay before bots can enter the game
    parser.add_argument("-d", "--bots-delay", help="Delay bots entry in sec.", dest="bots_delay",
                        action="store", default=10, type=int, choices=list(range(0, 59)), metavar="0-59",
                        nargs="?")
    # maximal number of bots in free gametypes
    parser.add_argument("-F", "--max-free", help="Maximum number of free bots", dest="max_free", action="store",
                        default=cfg["bots_max_nb"], type=int, choices=list(range(0, cfg["bots_max_nb"] + 1)),
                        metavar="0-" + str(cfg["bots_max_nb"]), nargs="?")
    # maximal number of bots in blue-team in team gametypes
    parser.add_argument("-B", "--max-blue", help="Maximum number of blue bots", dest="max_blue", action="store",
                        default=cfg["bots_max_nb"], type=int, choices=list(range(0, cfg["bots_max_nb"] + 1)),
                        metavar="0-" + str(cfg["bots_max_nb"]), nargs="?")
    # maximal number of bots in red-team in team gametypes
    parser.add_argument("-R", "--max-red", help="Maximum number of red bots", dest="max_red", action="store",
                        default=cfg["bots_max_nb"], type=int, choices=list(range(0, cfg["bots_max_nb"] + 1)),
                        metavar="0-" + str(cfg["bots_max_nb"]), nargs="?")
    # bot selection for free gametypes
    parser.add_argument("-f", "--free", help="Free bot or bot selection.", dest="free", type=str,
                        action="append", default=list(), nargs="?", metavar="crash,doom,...")
    # bot selection joining blue-team for team gametypes
    parser.add_argument("-b", "--blueteam", help="Bot or bot selection joinging blue team.", dest="blue", type=str,
                        action="append", default=list(), nargs="?", metavar="crash,doom,...")
    # bot selection joining red-team for team gametypes
    parser.add_argument("-r", "--redteam", help="Bot or bot bot selection joinging red team.", dest="red", type=str,
                        action="append", default=list(), nargs="?", metavar="crash,doom,...")
    # mod configuration
    parser.add_argument("mod", help="quake3 mod", action="store", default=cfg["mod_default"],
                        nargs="?", metavar="MOD")

    options = parser.parse_args()
    return options

def check_options_basic(opts: argparse.Namespace, cfg: dict) -> bool:
    if opts.game_type in cfg["gametypes_teams_builtin"] and len(opts.free) > 0:
        print(f"ERROR: Invalid configuration: free bots selection in team gametype: {opts.game_type}")
        return True
    if opts.game_type in cfg["gametypes_nonteams_builtin"] and (len(opts.blue) > 0 or len(opts.red)):
        print(f"ERROR: Invalid configuration: red/blue team bots selection in team gametype: {opts.game_type}")
        return True
    return False

def parse_bot_selection_instance(opt: str) -> (bool,str,str):
    opt_fields = opt.split(":")
    if len(opt_fields) < 1 or len(opt_fields) > 2:
        print(f"ERROR: Invalid configuration: Illegal number of instance instance fields: {opt}")
        return True, "", ""  # syntax error
    if len(opt_fields) == 1 or opt_fields[1] == "":
        return False, opt_fields[0], "+0"
    match = p_bs.search(opt_fields[1])
    if match:
        return False, opt_fields[0], match[0]
    else:
        print(f"ERROR: Invalid configuration: Illegal skillset code: {opt_fields[1]}")
        return True, "", "" # syntax error

def parse_bot_selection_instances(opt_fields: list) -> (bool,dict()):
    instances = dict()
    if (len(opt_fields) < 2) or opt_fields[1] == "":
        return False, dict()
    subopt_fields = opt_fields[1].split(",")
    for f in subopt_fields:
        invalid, nickname, skillset = parse_bot_selection_instance(f)
        if invalid:
            return True, dict() # syntax error
        if nickname in instances:
            print(f"ERROR: Invalid configuration: Multiple usage of nickname {nickname} in :{opt_fields[1]}")
            return True, dict()  # syntax error
        instances[nickname] = skillset
    return False, instances

def parse_bot_selection_mutliply_factor(opt_fields: list) -> int:
    if (len(opt_fields) < 3) or opt_fields[2] == "":
        return 1
    match = p_mf.search(opt_fields[2])
    if match:
        return int(match[0])
    else:
        print(f"ERROR: Invalid configuration: Ilegal multiplier factor: {opt_fields[2]}")
        return 0

def parse_bot_selection(opt: str) -> (bool,str, dict(), int):
    opt_fields = opt.split("/")  # <bot_or_team>/<instance nicknames list>/multiply_factor
    botname = opt_fields[0]
    invalid, instances = parse_bot_selection_instances(opt_fields)
    if invalid:
        return True, "", dict(), 0
    multiplier = parse_bot_selection_mutliply_factor(opt_fields)
    if multiplier < 1:
        return True, "", dict(), 0
    return False, botname, instances, multiplier


def parse_bot_selections(opts: argparse.Namespace, cfg: dict) -> (bool, dict()):
    bot_selections = {'free': list(), 'blue': list(), 'red': list(), }
    if opts.game_type in cfg["gametypes_teams_builtin"]:
        fbr_bot_options = {'blue': opts.blue, 'red': opts.red}
    else:
        fbr_bot_options = {'free': opts.free}
    for opt, opt_val in fbr_bot_options.items():
        for elem in opt_val:
            invalid, botname, instances, multiplier = parse_bot_selection(elem)
            if invalid:
                print(f"ERROR: Invalid configuration: bot selection ({opt}, gametype {opts.game_type}): {elem}")
                return True, dict()
            bot_selections[opt].append((botname, instances, multiplier))
    return False, bot_selections

def expand_mapmods_with_default_variant(mapgroupmods: dict, mod_default: 'str') -> None:
    mapgroupmods_exp = dict()
    for m, c in mapgroupmods.items():
        for vm, vm_ext in c['variantmods'].items():
            mapgroupmods_exp[m + '_' + vm_ext] = {'basemod': mod_default, 'variantmod': dict()}
            mapgroupmods_exp[m + '_' + vm_ext]['gametypes'] = c['gametypes']
            mapgroupmods_exp[m + '_' + vm_ext]['maps'] = c['maps']
    mapgroupmods.update(mapgroupmods_exp)
    return

def check_mod_and_compatible_gametypes(mod: str, gametype: str, cfg: dict, cfg_definitions: dict) -> (bool, tuple):
    if mod == cfg["mod_default"]:
        compatible_gamettypes = tuple(cfg["gametypes_builtin"])
    elif mod in cfg_definitions["mods"]:
        compatible_gamettypes = tuple(cfg_definitions["mods"][mod])
    elif cfg_definitions["mapgroupmods"][mod]['basemod'] == cfg["mod_default"]:
        compatible_gamettypes = tuple(cfg["gametypes_builtin"])
    elif cfg_definitions["mapgroupmods"][mod]['basemod'] in cfg_definitions["mods"]:
        compatible_gamettypes =  tuple(set( cfg_definitions["mods"][cfg_definitions["mapgroupmods"][mod]['basemod']]) & \
                     set(cfg_definitions["mapgroupmods"][mod]['gametypes']))
    else:
        print(f"ERROR: Invalid configuration: Unsupported mode: {mod}")
        return True, tuple([])
    if not(gametype in compatible_gamettypes):
        errmsg = f"ERROR: Invalid configuration: Unsupported game type {gametype} for mod {mod}."
        errmsg += f"Compatible gametypes: {list(compatible_gamettypes)}"
        print(errmsg)
        return True, tuple([])
    return False, compatible_gamettypes

def check_maps(maps: list, mod: str, gametype: str, compatible_gametypes: tuple,cfg: dict, mapgroupmods: dict) -> bool:
    for m in maps:
        if m in cfg['maps_builtin'][gametype]:
            continue
        if m in mapgroupmods[mod]["maps"] and gametype in compatible_gametypes:
            continue
        else:
            print(f"ERROR: Invalid configuration: Unsuported map {m} for couple mod/gametype: {mod}/{gametype}")
            return True
    return False

def upper_lim_selections(nb, nr, bots_max_nb):
    o = max(0, nb + nr - bots_max_nb)
    d = nb - nr
    d_br = max(0, d)
    d_rb = max(0, -d)
    nb = nb - d_br + max(0, d_br - o)
    nr = nr - d_rb + max(0, d_rb - o)
    o = max(0, nb + nr - bots_max_nb)
    nb = nb - int(o / 2)
    nr = nr - int(o / 2)
    return nb, nr

def get_skill(skill: int) -> str:
    if skill >= 0:
        return '+' + str(skill)
    else: # skill < 0:
        return '-' + str(abs(skill))

def set_skill(skill: str, g_spskill: int) -> str:
    if skill[0] == '+':
        return str(min(5,max(0,g_spskill + int(skill[1:]))))
    elif skill[0] == '-':
        return str(min(5,max(0,g_spskill - int(skill[1:]))))
    else:
        return str(min(5,max(0,int(skill[1:]))))


def insert_selection(selection: list, remaining_slot_nb: int) -> list:

    selection_members_nb = len(selection["members"])
    slots_not_allocated_nb = max(0,remaining_slot_nb - selection_members_nb) * int(selection["repeat"])
    selection_members = list(selection["members"].keys())
    random.shuffle(selection_members)
    selection_members = selection_members[0:remaining_slot_nb]
    bots_selected = []
    bot_cnt = 0
    K = math.ceil(slots_not_allocated_nb / selection_members_nb)
    s = int(K>0)
    if slots_not_allocated_nb>selection_members_nb:
        z = 1 + math.floor(math.log10(math.floor(slots_not_allocated_nb / selection_members_nb)))
    else:
        z = 1
    for k in range(0,K+1,1):
        for member in selection_members:
            sfx = str(k).zfill(z) * s
            bots_selected.append([
                selection["members"][member]['bot'],
                member + sfx,
                get_skill(selection["members"][member]['deltaskill'])])
            bot_cnt = bot_cnt + 1
            if bot_cnt == remaining_slot_nb:
                return bots_selected
    return bots_selected
def insert_bot(bot_settings: list, bot_max_nb) -> list:

    n_nicknames = len(bot_settings[1])
    if n_nicknames == 0:
        n_nicknames = 1
        bot_settings[1][bot_settings[0]] = '+0'
    z = 1+math.floor(math.log10(bot_settings[2]))
    team_members = []
    bot_cnt = 0
    s = int(bot_settings[2] > 1)
    for k in range(0, bot_settings[2], 1):
        for nickname, skillset in bot_settings[1].items():
            if bot_cnt == bot_max_nb:
                return team_members
            sfx = str(k).zfill(z) * s
            team_members.append([bot_settings[0], nickname+sfx, skillset])
            bot_cnt = bot_cnt + 1
    return team_members

def check_bots_and_selections(gametype: str, bot_max_nb: dict(), bot_selections: dict,
                              cfg: dict, cfg_definitions: dict) -> (bool, dict()):
    team_labels = ['free','blue','red']
    teams = dict(zip(team_labels,[[],[],[]]))
    for label in team_labels:
        for elem in bot_selections[label]:
            if elem[0] in cfg['bots_builtin']:
                selection = insert_bot(elem, bot_max_nb[label])
            elif elem[0] in cfg_definitions['bots']:
                if gametype in cfg["gametypes_teams_builtin"] and not(cfg_definitions['bots'][elem[0]]['team']):
                    print(f"ERROR: Invalid configuration: Bot {elem[0]} not compatible with gametype: {gametype}")
                    return True, dict()
                else:
                    selection = insert_bot(elem, bot_max_nb[label])
            elif elem[0] in cfg_definitions['botselections']:
                continue # process at pass2
            else:
                print(f"ERROR: Invalid configuration: Unsupported bot {elem[0]}.")
                return True, dict()
            teams[label].extend(selection)
            if len(teams[label]) > bot_max_nb[label]:
                break

    for label in team_labels:
        for elem in bot_selections[label]:
            if elem[0] in cfg['bots_builtin']:
                continue # processed in pass 1
            elif elem[0] in cfg_definitions['bots']:
                continue  # processed in pass 1
            elif elem[0] in cfg_definitions['botselections']:
                remaining_team_slot = bot_max_nb[label] - len(teams[label])
                teams[label].extend(insert_selection(cfg_definitions['botselections'][elem[0]], remaining_team_slot))
                if len(teams[label]) > bot_max_nb[label]:
                    break
            else:
                pass # error already trapped during pass 1
                print(f"ERROR: Invalid configuration: Unsupported bot/bot selection {elem[0]}.")
                return True, dict()

    for label in team_labels:
        L = min(len(teams[label]),bot_max_nb[label])
        teams[label] = teams[label][0:L]

    nb = len(teams["blue"])
    nr = len(teams["red"])
    nb, nr = upper_lim_selections(nb, nr, cfg["bots_max_nb"])
    del teams["blue"][nb:]
    del teams["red"][nr:]

    return False, teams

def gen_customserver_config(teams: dict, cfg: dict, cfg_directives: dict,
                            gametype: str, maps: list, g_spskill: int, bots_delay: int, bot_minplayers:int ) -> list:
    customserver = []

    for k in cfg["configuration_nongames_files"]:
        customserver.extend(cfg_directives[k])
    customserver.extend(cfg_directives[gametype])

    maps_nb = len(maps)
    maps_directives = list()
    for k, m in enumerate(maps):
        maps_directives.append(' '.join(
            ['set d' + str(1 + k), '"map', m, '; set nextmap vstr', 'd' + str(1 + ((k + 1) % maps_nb)) + '"']))
    maps_directives.append('vstr d1')
    customserver.extend(maps_directives)

    bots_directives = [' '.join(['seta', 'g_spskill', str(g_spskill)])]

    if gametype in cfg["gametypes_nonteams_builtin"]:
        for bot in teams["free"]:
            bots_directives.append(' '.join(['addbot',
                                             bot[0], set_skill(bot[2], g_spskill), "free", str(bots_delay), bot[1]]))
    else:
        # interleave
        Lb = len(teams["blue"])
        Lr = len(teams["red"])
        L = min(Lb,Lr)
        bias = int(Lr < Lb) + int(Lr == Lb)*(int(str((time.time()))[-1])>5) # shorter team first, random if even
        for idx, elem in enumerate(zip(list(zip(teams["blue"][0:L],["blue"]*L)),
                                       list(zip(teams["red"][0:L], ["red"]*L)))):
            pair = list(elem)
            if (idx+bias) % 2 == 1:
                pair.reverse()
            for bot,team_label in pair:
                bots_directives.append(' '.join(['addbot',
                                                 bot[0], set_skill(bot[2], g_spskill), team_label, str(bots_delay),
                                                 bot[1]]))
        for team_label in ["blue","red"]:  # include rest of longer team
            for bot in teams[team_label][L:]:
                bots_directives.append(' '.join(['addbot',
                                             bot[0], set_skill(bot[2], g_spskill), team_label, str(bots_delay),
                                             bot[1]]))


    bots_directives.append(' '.join(['seta', 'bot_minplayers', str(bot_minplayers)]))
    customserver.extend(bots_directives)


    return customserver

def q3asrv() -> int:

    cfg = get_q3asrv_config()
    opts = argument_parsing(cfg)

    if check_options_basic(opts, cfg):
        return 1

    err, bot_selections = parse_bot_selections(opts, cfg)
    if err:
        return 1

    cfg_directives = dict()
    for f, p in cfg["files_directives"].items():
        cfg_directives[f] = read_cfg_file(p)

    cfg_definitions = dict()
    for f, p in cfg["files_definitions"].items():
        cfg_definitions[f] = read_toml_file(p)

    expand_mapmods_with_default_variant(cfg_definitions['mapgroupmods'], cfg["mod_default"])

    err, compatible_gametypes = check_mod_and_compatible_gametypes(opts.mod, opts.game_type, cfg, cfg_definitions)
    if err:
        return 1

    if check_maps(opts.maps, opts.mod, opts.game_type,
                  compatible_gametypes, cfg, cfg_definitions['mapgroupmods']):
        return 1

    err, teams = check_bots_and_selections(opts.game_type,
                                           {"free": opts.max_free, "blue": opts.max_blue, "red": opts.max_red},
                                           bot_selections,
                                           cfg,
                                           cfg_definitions)
    if err:
        return 1

    customserver = gen_customserver_config(teams, cfg, cfg_directives,
                                           opts.game_type, opts.maps, opts.g_spskill,
                                           opts.bots_delay, opts.bot_minplayers)


    if write_cfg_file(str(cfg['file_fs_game']), [opts.mod]):
        return 1
    customserver_filename = (pathlib.Path.home()).joinpath(cfg["path_q3a"], opts.mod, cfg["file_customserver"])
    if write_cfg_file(str(customserver_filename), customserver):
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(q3asrv())
