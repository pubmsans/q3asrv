#!/bin/bash
# quick starting a preconfigured Quake3 Arena dedicated server

echo "-------------------------------"
echo "Starting Q3A server, game mode:"
echo "-------------------------------"

FSGAME=`cat ~/.q3a/.fs_game.q3asrv`
UNIXFLAVOR=`cat ~/.q3a/.unix_flavor`

if [ "$UNIXFLAVOR" = "debian" ]; then
  BIN="/usr/lib/ioquake3/ioq3ded"
elif [ "$UNIXFLAVOR" = "arch" ]; then
  BIN="/opt/quake3/ioq3ded"
else
    echo "ERROR (q3arun.sh): unknown unix flavor: $UNIXFLAVOR"
    exit 1
fi;
$BIN +set fs_game $FSGAME +exec customserver.cfg
exit 0